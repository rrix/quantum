/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2013  Ryan Rix <ry@n.rix.si>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "adnunifiedtimelinemodel.h"

#include <QNetworkAccessManager>
#include <qjson/parser.h>

#include <KDebug>

ADNUnifiedTimelineModel::ADNUnifiedTimelineModel(QObject* parent): QStandardItemModel(parent),
                                                                   m_qnam(new QNetworkAccessManager())
{
}

ADNUnifiedTimelineModel::ADNUnifiedTimelineModel(QObject* parent, QNetworkAccessManager* qnam): QStandardItemModel(parent)
{
  m_qnam = qnam;
}

void ADNUnifiedTimelineModel::refreshData()
{
  // Construct Request
  //QNetworkRequest request = QNetworkRequest(QUrl("https://stream-channel.app.net/stream/user"));
  QNetworkRequest request = QNetworkRequest(QUrl("https://alpha-api.app.net/stream/0/posts/stream/unified"));
  request.setRawHeader("Authorization", "Bearer AQAAAAAACIpSfKwgja4npZkx19wXthmupkNo1XaE9W_p5z3KCeikwXW-sLcWUh8pRBZOxiHMysaTvqnQjMY8Nrnx2hJbidiMUA");
  
  m_reply = m_qnam->get(request);
  connect(m_reply, SIGNAL(finished()),
	  this,    SLOT(newData()));
  connect(m_reply, SIGNAL(error(QNetworkReply::NetworkError)),
	  this,    SLOT(handleError(QNetworkReply::NetworkError)));
  m_data = new QVariantList();
}

void ADNUnifiedTimelineModel::newData()
{
  QByteArray data = m_reply->readAll();
  
  QJson::Parser parser;
  bool ok;
  
  QVariant response_object = parser.parse(data, &ok);
  QVariantList post_list = response_object.toMap().take("data").toList();
  m_data->clear();
  for(int i = 0; i < post_list.length(); i++) {
    QString text = post_list[i].toMap().value("text").toString();
    QStandardItem* newItem = new QStandardItem(text);
    
    appendRow(newItem);
  }
  
  emit dataChanged(index(0,0), index(post_list.length(),0));
}

void ADNUnifiedTimelineModel::handleError(QNetworkReply::NetworkError thrownError)
{
  kDebug() << thrownError;
  emit error(thrownError);
}

#include "adnunifiedtimelinemodel.moc"
