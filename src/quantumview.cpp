/***************************************************************************
 *   Copyright (C) 2013 by Ryan Rix <ry@n.rix.si>                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "quantumview.h"
#include "adn/adnstreamsapi.h"
#include "adn/adnunifiedtimelinemodel.h"
#include "settings.h"

#include <KLocale>
#include <KStandardDirs>

#include <QApplication>
#include <QtGui/QLabel>
#include <QGraphicsObject>
#include <QtDeclarative/QDeclarativeView>
#include <QtDeclarative/QDeclarativeProperty>
#include <QtDeclarative/QDeclarativeItem>
#include <QtDeclarative/QDeclarativeContext>

quantumView::quantumView(QWidget *)
{ 
    view = new QDeclarativeView(this);
    
    m_adnStreamsApi = new ADNStreamsAPI();
    QDeclarativeContext* context = view->rootContext();
    ADNUnifiedTimelineModel* timelineModel = m_adnStreamsApi->unifiedTimelineModel();
    timelineModel->refreshData();
    context->setContextProperty("unifiedTimelineModel", timelineModel);
    
    view->setSource(QUrl::fromLocalFile(KStandardDirs::locate("data", "quantum/qml/main.qml")));
    kcfg_sillyLabel = view->rootObject();
    kcfg_sillyLabel->setProperty("width", width());
    QDeclarativeProperty(kcfg_sillyLabel, "width").write(width());
    kcfg_sillyLabel->setProperty("height", height());
    QDeclarativeProperty(kcfg_sillyLabel, "height").write(height());
    QObject::connect(kcfg_sillyLabel, SIGNAL(clicked()),
                     qApp,            SLOT(quit()));
    
    view->show();
}

quantumView::~quantumView()
{
}

#include "quantumview.moc"
