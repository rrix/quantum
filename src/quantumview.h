/***************************************************************************
 *   Copyright (C) 2013 by Ryan Rix <ry@n.rix.si>                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef QUANTUMVIEW_H
#define QUANTUMVIEW_H

#include <QtGui/QWidget>
#include <QtGui/QLabel>
#include <QtDeclarative/QDeclarativeView>

class QDeclarativeView;
class ADNStreamsAPI;

/**
 * This is the main view class for quantum.  Most of the non-menu,
 * non-toolbar, and non-statusbar (e.g., non frame) GUI code should go
 * here.
 *
 * @short Main view
 * @author Ryan Rix <ry@n.rix.si>
 * @version %{VERSION}
 */

class quantumView : public QDeclarativeView
{
    Q_OBJECT
public:
    /**
     * Default constructor
     */
    quantumView(QWidget *parent);

    /**
     * Destructor
     */
    virtual ~quantumView();

    QDeclarativeView *view;
    QObject *kcfg_sillyLabel;
    
signals:
    /**
     * Use this signal to change the content of the statusbar
     */
    void signalChangeStatusbar(const QString& text);

    /**
     * Use this signal to change the content of the caption
     */
    void signalChangeCaption(const QString& text);
    
    void clicked();
    
private:
  ADNStreamsAPI* m_adnStreamsApi;
};

#endif // QUANTUMVIEW_H
