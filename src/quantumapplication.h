/***************************************************************************
 *   Copyright (C) 2013 by Ryan Rix <ry@n.rix.si>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/
#ifndef QUANTUM_H
#define QUANTUM_H


#include <KXmlGuiWindow>

class quantumView;
class KToggleAction;
class KUrl;

/**
 * This class serves as the main window for quantum.  It handles the
 * menus, toolbars and status bars.
 *
 * @short Main window class
 * @author Ryan Rix <ry@n.rix.si>
 * @version %{VERSION}
 */
class QuantumApplication : public KXmlGuiWindow
{
    Q_OBJECT
public:
    /**
     * Default Constructor
     */
    QuantumApplication();

    /**
     * Default Destructor
     */
    virtual ~QuantumApplication();

private:
    quantumView *m_view;

    KToggleAction *m_toolbarAction;
    KToggleAction *m_statusbarAction;
};

#endif // _quantum_H_
